class @Message extends React.Component
  @propTypes =
    body: React.PropTypes.string

  render: ->
    <div>
      <div>Title: { @props.title }</div>
      <div>Body: { @props.body }</div>
    </div>
