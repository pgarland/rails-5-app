class @Panel1 extends React.Component
  @propTypes =
    title: React.PropTypes.string
    body: React.PropTypes.string

  render: ->
    <div>
      <div>Title: {this.props.title}</div>
      <div>Body: {this.props.body}</div>
    </div>
