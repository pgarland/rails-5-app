class Panel2 extends React.Component {
  render () {
    return (
      <div>
        <div>Title: {this.props.title}</div>
        <div>Body: {this.props.body}</div>
      </div>
    );
  }
}

Panel2.propTypes = {
  title: React.PropTypes.string,
  body: React.PropTypes.string
};
