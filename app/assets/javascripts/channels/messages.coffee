App.messages = App.cable.subscriptions.create('ChatChannel',
  received: (data) ->
    @renderMessage(data)

  renderMessage: (data) ->
    ReactDOM.render(
      React.createElement(Message, title: 'rendered via react!!!', body: data.body),
      document.getElementById('message-container')
    )
)
