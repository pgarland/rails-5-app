class MessagesController < ApplicationController

  def create
    message_body = message_params.fetch('body')
    ActionCable.server.broadcast 'messages', body: message_body, head: :ok
  end

  private

  def message_params
    params.require(:message).permit(:body)
  end

end
