class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :instantiate_message

  def instantiate_message
    @message = Message.new
  end

end
