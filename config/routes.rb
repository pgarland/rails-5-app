Rails.application.routes.draw do
  mount ActionCable.server => '/sockets'
  post 'messages/create' => 'messages#create', as: :messages

  root 'dashboard#home'

end
