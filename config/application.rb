require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsApp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    redis_config = {
      host: '127.0.0.1',
      port: 6379,
      expires_in: 90.minutes
    }
    config.cache_store = :redis_store, redis_config.merge(namespace: 'rails-app')
    config.session_store :redis_store, servers: redis_config.merge(namespace: 'rails-session')

    config.generators do |g|
      g.orm             :active_record
      g.template_engine :slim
      g.test_framework  :rspec
      g.stylesheets     false
      g.javascripts     false
      g.helper          false
    end

  end
end
