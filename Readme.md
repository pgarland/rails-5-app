# Rails 5, React, Redis, ActionCable demo

### Install Redis (unless you already have it)
1. `brew install redis`
1. `redis-server /usr/local/etc/redis.conf`

### Application Setup
1. `bundle install`
1. `rake db:create` (must have postgres setup)
1. `rails s`
1. in another terminal: `redis-cli monitor`
1. enter message into input, press enter, watch application logs

### Gems

- https://github.com/redis-store/redis-rails
- https://github.com/reactjs/react-rails
- https://github.com/slim-template/slim-rails

